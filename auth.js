const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

/*
Notes:
	
	JWT 
		- way to securely pass info fr one part of a server to the frontend or other parts of the application
		-allows us to authorize our users to access or disallow access to certain parts of our app
		- handles if a user is Admin or not

		-like a gift wrapping service that is able to encode user details which can only be unwrapped by JWT's own methods and if the secret provided is intact

		If the JWT seems tampered, we will reject the user's attempt to access a feature

		3 steps:
		1. create token 
		2. sign
		3. verify
*/

module.exports.createAccessToken = (user) => {
	// contains details of user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// sign - 3 arguments: 
	return jwt.sign(data, secret, {})

};

/*
Notes:
	You can only get a unique JWT with our secret if you log in to our app with the correct email and pw

	as a user, you can only get your own details from your own token for logging in

	JWT is not meant to store sensitive data. for now, for ease of use, we add the email and isAdmin details of the logged in user. however, in the future, you can limit this to only id and for every route and feature, you can simply look up for the user in the database to get his details

	We will verify the legitimacy of aJWT every time a user access a restricted feature. each JWT contains a secret only our server knows. IF the JWT has been changed in any way, we will reject the user and his tampered token. IF the JWT does not contain a secret OR the secret is different, we will reject his access and token


*/

module.exports.verify = (req, res, next) => {
	// req.headers.authorization will contain the token
	let token = req.headers.authorization
	console.log(token)

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"})

	} else {
		 token = token.slice(7, token.length)

		 // Bearer eyJhgjkdajdoaipotjpqowjmql

		 jwt.verify(token, secret, (err, decodedToken) => {

		 	if(err){
		 		return res.send({
		 			auth: "Failed",
		 			message: err.message
		 		})

		 	} else {
		 		req.user = decodedToken;

		 		// will let us go to the middleware
		 		next();
		 	}
		 })
	}
};

// decode
module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Forbidden Action"
		})
	}
};

