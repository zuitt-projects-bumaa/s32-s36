// import bcrypt module to store password with more security
const bcrypt = require("bcrypt");

// import user model
const User = require("../models/User");

//import course model
const Course = require("../models/Course");

// import auth module
const auth = require("../auth");

// Controllers 

// User Registration

module.exports.registerUser = (req, res) => {
	console.log(req.body);

	// bcrypt will add a layer of security to your user's pw. it randomizes our password into a randomized character version of the original string. it has 2 parameters: where, saltRounds (how many times will it randomize)

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	// create new user document

	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));

};

// retrieve all users

module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// login

module.exports.loginUser = (req, res) => {

	console.log(req.body); 

	/*
		1. find user by email
		2. if found, check pw
		3. if not found, send message
		4. if matched, generate key
			if not, end message
	*/
	
	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null) {
			return res.send("The user does not exist");

		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {

				return res.send("Password is incorrect")
			}
		}
	})
	.catch(err => res.send(err));
};

// get user details 

module.exports.getUserDetails = (req, res) => {

	console.log(req.user)

	// 1. find a logged in user's doc from db and send it to client by id

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// find by email 

module.exports.checkEmailExists = (req, res) => {
	console.log(req.body)

	User.findOne({email: req.body.email})
	.then(foundEmail => {
		if(foundEmail === null) {
			return res.send("Email is available")
		} else {
			return res.send("Email is already registered")
		}
	})
	.catch(err => res.send(err))

};

// admin user: updating reg user to admin

module.exports.updateAdmin = (req, res) => {
	// check if logged in
	console.log(req.user.id);
	// check id designated in params
	console.log(req.params.id);
	

	let updates = {
		isAdmin: true 
	}
	//has 3 arguments: id, updates, option
 	// option new: true allows us to return the updated version of the document we were updating. By default, without this argument, findByIdAndUpdate will return the previous state of the document or previous version 
	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
}

// update user details

module.exports.updateUserDetails = (req, res) => {
	console.log(req.body); // check input for new values for our user's details
	console.log(req.user.id); // check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};


// Enroll a user

module.exports.enroll = async (req, res) => {
	console.log(req.body)

	/*
		Enrollment Process:

		1. Look for the user by its id.
			>> push the details of the course we're trying to enroll in.
				>> we'll push to a new enrollment subdocument in our user

		2. Look for the course by its id.
			>> push the details of the enrollee/ user who's trying to enroll.
				>> we'll push to a new enrollees subdocument in our course

		3. When both saving documents are successful, we send a message to the client 
	*/

	// console.log(req.user.id);
	// console.log(req.body.courseId);

	// Checking if a user is an admin, if he is an admin then he should not be able to enroll

	if(req.user.isAdmin){
		return res.send("Action Forbidden");

	};

	/*
		Find the user:

		async- a keyword that allows us to make our function asynchronous. Which means, that instead of JS regular behavior of running each code line by line, it will allow us to wait for the result of the function.

		await- a keyword that allows us to wait for the function to finish before proceeding

	*/

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		
		// add the courseId in an object and push that object into user's enrollment array
		let newEnrollment = {
			courseId: req.body.courseId
		}
		console.log('newEnrollment', newEnrollment);


		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(err =>
		err.message);
	})

	// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with our message
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}
	console.log('req', req.body);

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		console.log('course', course);

		// create an object which will contain the user id of the enrollee of a course
		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => {
			return err.message;
		})
	});

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	};

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: 'Enrolled Successfully!'})
	};

}; 

// activity 5: get enrollments

module.exports.getEnrollments = (req, res) => {
console.log(req.user);

	User.findById(req.user.id)
	.then(result => {
		console.log(result)
		res.send(result.enrollments)
		})
	.catch(err => res.send(err));
};
