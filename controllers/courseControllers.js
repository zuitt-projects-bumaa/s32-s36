const Course = require("../models/Course");

module.exports.addCourse = (req, res) => {
	//to check if data is getting thru
	console.log(req.body);

// create new course document

	let newCourse = new Course ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,	
	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));

};

// get all courses

module.exports.getAllCourses = (req, res) => {
	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// get single course

module.exports.getSingleCourseController = (req, res) => {
	console.log(req.params);

 	Course.findById(req.params.id)
 	.then(result => res.send(result))
 	.catch(error => res.send(error))
};


// activity 4 

// archive course 

module.exports.archiveCourse = (req, res) => {
	console.log(req.params);
	console.log(req.params.id);

	let updates = {
		isActive: false
		}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
};

// activate course

module.exports.activateCourse = (req, res) => {
	console.log(req.params);

	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
};

// get active courses

module.exports.getActiveCourses = (req, res) => {
	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// update course

module.exports.updateCourse = (req,res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};

// get inactive courses

module.exports.getInactiveCourses = (req, res) => {

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// find courses by name

module.exports.findCoursesByName = (req, res) => {

	console.log(req.body);

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0) {
			return res.send("No courses found");
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};


// find courses by price

module.exports.findCoursesByPrice = (req, res) => {

	Course.find({price: req.body.price})
	.then(result => {

		console.log('result', result)
		// meaning nothing is found
		if(result.length === 0) {
			return res.send("No course found");
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// activity 5: get enrollees

module.exports.getEnrollees = (req, res) => {

	console.log(req.params);

	Course.findById(req.params.id)
	.then(result =>  {
		console.log(result)
		res.send(result.enrollees)
	})
	.catch(err => res.send(err));
};