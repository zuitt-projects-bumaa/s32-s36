const express = require ('express');
const mongoose = require('mongoose');

// allows our backend app to be available in our frontend app
// cors - cross origin resource sharing
// to avoid errors on connection
// requiring is not yet using. to use, add middleware
const cors = require('cors');

const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
const port = 4000;

const app = express(); 


mongoose.connect("mongodb+srv://Admin_Bumaa:admin169@cluster0.cvssk.mongodb.net/bookingAPI169?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));


// middleware
app.use(express.json());

//use our routes and group together under '/users' and '/courses'
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);
app.use(cors());



app.listen(port, () => console.log(`Server is running at port ${port}` ));