const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");

const auth  = require("../auth");

const {verify, verifyAdmin} = auth;
// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Get all Users
router.get("/", userControllers.getAllUsers);

// Login

router.post("/login", userControllers.loginUser)


// retrieve user details
router.get("/getUserDetails", verify, userControllers.getUserDetails);

// check if email exists
router.post('/checkEmailExists', userControllers.checkEmailExists);


// mini activity: update reg user to admin

router.put('/updateAdmin/:id', verify,verifyAdmin,userControllers.updateAdmin);
//:id to capture from params


// update user details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);


// Enroll registered user
router.post("/enroll", verify, userControllers.enroll);

// activity 5: get enrollments
router.get("/getEnrollments", verify, userControllers.getEnrollments);



module.exports = router; 


