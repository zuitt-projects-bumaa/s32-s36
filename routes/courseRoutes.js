const express = require('express');
const router = express.Router();
const auth = require("../auth");

//destructure auth.js 
// auth is an imported module, so there is an object in JS
const {verify, verifyAdmin} = auth;


const courseControllers = require("../controllers/courseControllers");

// add new course
router.post("/", verify, verifyAdmin, courseControllers.addCourse);

// get all courses
router.get("/", courseControllers.getAllCourses);

// retrieve single course route 
router.get('/getSingleCourse/:id', courseControllers.getSingleCourseController);


// activity 4

router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse);

router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);

router.get('/getActiveCourses', courseControllers.getActiveCourses);

// update a course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

// get inactive courses
router.get("/getInactiveCourses", verify,verifyAdmin, courseControllers.getInactiveCourses);

// find courses by names
router.get("/findCoursesByName", courseControllers.findCoursesByName);

// find courses by price
router.get("/findCoursesByPrice", courseControllers.findCoursesByPrice)

router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees)

module.exports = router; 



/*
Activity 2:

	>> Create a new route and controller which will allow us to add/create a new Course document.
		-endpoint: '/'

		>> In courseRoutes.js:
		-Import express and save it in a variable called express.
		-Save the Router() method of express in a variable called router.
		-Import your courseControllers from your courseControllers file.
		-Create your route for course creation.

		>> Go back to your courseControllers.js file:
		-Import your Course model in the courseController.js file.
		-Add a new controller called addCourse which will allow us to add a course: 
		-Create a new course document out of your Course model.
		-Save your new course document.
			-Then send the result to the client.
			-Catch the error and send it to our client.

		>> Back in courseRoutes.js: 
		-Add the addCourse controller in your route.

	>> Create a new route and controller which will allow us to get all Course documents.
		-endpoint: '/'

		>> In courseRoutes.js:
		-Create a route to get all course documents.

		>> Go back to your courseControllers.js file:
		-Add a controller called getAllCourses which will allow us to find all course documents.
		-use the find() method of our User model to find our documents.
			-Then send the result to the client.
			-Catch the error and send it to our client.

		>> Back in courseRoutes.js:
		-Add the getAllCourses controller in your route.


		>> Remember to:
		-import your courseRoutes in index.js
		-Add middleware to group your courseRoutes under '/courses'

	>> Test the routes in Postman
*/

